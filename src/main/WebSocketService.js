"use strict";

angular.module("nsWs", []).service("$ws", function(
    $q,
    $timeout,
    $rootScope
) {
    var self,
        connectedMsg,
        deferred,
        socket,
        stompClient;

    self = this;

    function connect() {
        deferred = $q.defer();
        socket = new SockJS("url//ws"); // zrobic inicjalizacje przez provide
        stompClient = self.$$stompClient = Stomp.over(socket);
        stompClient.debug = null; // turn of debug logging
        stompClient.connect({},
            function(frame) {
                if (connectedMsg) {
                    self.log(connectedMsg);
                    connectedMsg = null;
                }
                deferred.resolve(frame);
            },
            function() { // error callback
                // stomp.js:330 - msg = "Whoops! Lost connection to " + _this.ws.url;
                if (!connectedMsg) {
                    self.log(moment().format("YYYY-MM-DD hh:mm:ss") + " - WebSocket connection lost");
                }
                $timeout(function() {
                    self.log(moment().format("YYYY-MM-DD hh:mm:ss") + " - Retry WebSocket connection");
                    connectedMsg = moment().format("YYYY-MM-DD hh:mm:ss") + " - WebSocket connection restored";
                    self.reconnect();
                }, 5000);
            }
        );
    }

    connect();

    var subscriptions = [];
    this.reconnect = function reconnect() {
        connect();
        subscriptions.forEach(function(subscription) {
            registerSubscription(subscription);
        });
    };
    var registerSubscription = function(subscription) {
        var headers = subscription.config.headers;
        var callback = subscription.config.callback;
        var destination = subscription.config.destination;

        var stompCallback = function(message) {
            var obj;
            try {
                obj = JSON.parse(message.body);
            } catch (e) {
                throw new Error("bad JSON: \"" + message.body + "\"");
            }
            $rootScope.$apply(function() {
                callback(obj);
            });
        };

        deferred.promise.then(function() {
            var stompSubscription = stompClient.subscribe(destination, stompCallback, headers);
            angular.extend(subscription, stompSubscription);
        });
    };

    // ----------------------
    this.subscribe = function(destination, callback, headers) {
        var subscription = {
            config: {
                destination: destination,
                callback: callback,
                headers: headers
            },
            //TODO: ????????????wtf???
            unsubscribe: function() { }
        };
        registerSubscription(subscription);
        subscriptions.push(subscription);
        return subscription;
    };
    this.send = function(destination, headers, body) {
        body = (typeof body === 'object') ? JSON.stringify(body) : body;
        stompClient.send(destination, headers, body);
    };

    this.log = function(msg) {
        console.info(msg);
    };
});
