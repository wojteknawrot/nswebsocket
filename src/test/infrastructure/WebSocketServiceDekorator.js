"use strict";

angular.module("nsWs").config(function ($provide) {

    $provide.decorator("$ws", function($delegate, $rootScope) {

        var originalSubscribe = $delegate.subscribe;

        $delegate.subscribe = function(destination, callback, headers) {
            var ret = originalSubscribe.call(this, destination, callback, headers);
            // digest is needed for $ws.subscribe to proper work in tests
            // for case when $webSocketBackend is already connected
            $rootScope.$digest();
            return ret;
        };
        return $delegate;
    });
});

