"use strict";

angular.module("nsWs").config(function($provide) {

    $provide.service("$webSocketBackend", function($injector, $timeout) {

        var $ws;

        window.SockJS = function(url) {
            //console.log("new SockJS(" + url + ")");
            var self = this;
            // inspired by SockJS source
            var CONNECTING = 0;
            var OPEN = 1;

            var readyState = CONNECTING;

            // Simulate async SockJS connection
            $timeout(function() {
                readyState = OPEN;
                // console.log("SockJS - connection opened");
                self.onOpen();
            });

            /////////////////////////////////////
            this.onOpen = angular.noop;
            this.send = function(data) {
                if (readyState === CONNECTING) {
                    throw new Error("INVALID_STATE_ERR");
                }
                // mock doesn't send any real data
                return true;
            };
        };

        var subscribers = {};


        window.Stomp = {
            over: function(socket) {
                // when new connection is created it doesn't have any subscriptions
                subscribers = {};
                var errorCallback;

                return {
                    debug: angular.noop,
                    resetConnection: function() {
                        errorCallback();
                    },
                    connect: function(headers, connectCallback, $errorCallback) {
                        errorCallback = $errorCallback;
                        socket.onOpen = function() {
                            connectCallback();
                        };
                    },
                    send: angular.noop,
                    subscribe: function(destination, callback, headers) {
                        socket.send("SUBSCRIBE");

                        subscribers[destination] = subscribers[destination] || [];
                        var subscription = {
                            callback: callback,
                            headers: headers
                        };
                        subscribers[destination].push(subscription);

                        return {
                            unsubscribe: function() {
                                var idx = subscribers[destination].indexOf(subscription);
                                subscribers[destination].splice(idx,1);
                            }
                        };
                    }

                };
            }
        };

        $ws = $injector.get("$ws");
        $ws.log = angular.noop;

        // -----------
        // --- API ---
        // -----------
        this.resetConnection = function() {
            $ws.$$stompClient.resetConnection();
            return this;
        };
        /**
         * Simulate async SockJS connection
         */
        this.connect = function() {
            try {
                $timeout.flush();
            } catch(e) {
                new Error("Already connected");
            }
            return this;
        };
        this.publish = function(destination, body) {
            subscribers[destination].forEach(function(subscriber) {
                subscriber.callback({
                    command: "CONNECTED",
                    headers: {},
                    body: (typeof body === 'object') ? JSON.stringify(body) : body
                    // body: _(body).isObject() ? JSON.stringify(body) : body

                });
            });
            return this;
        };

        // ------------
        // --- init ---
        // ------------
        // Lets be connected :-)
        this.connect();
    });
});

