module.exports = function (config) {
    config.set({
        // base path, that will be used to resolve files and exclude
        basePath: '../../',

        frameworks: [ 'jasmine' ],

        //reporters: ['mocha'],

        mochaReporter: {
            output: 'autowatch',
            colors: {
                //success: 'blue',
                //info: 'bgGreen',
                //warning: 'cyan',
                error: 'bgRed'
            }
        },

        preprocessors: {
            "src/main/**/*.js": "coverage"
        },

        // list of files / patterns to load in the browser
        files: [
            'bower_components/angular/angular.js',
            'bower_components/moment/moment.js',
            'bower_components/sockjs/sockjs.js',
            'bower_components/stomp-websocket/lib/stomp.js',
            'src/main/**/*.js',

            'bower_components/angular-mocks/angular-mocks.js',
            'src/test/infrastructure/**/*.js',
            'src/test/spec/**/*.js'
        ],

        // list of files / patterns to exclude
        exclude: [],

        // web server port
        port: 8088,

        // level of logging
        // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
        logLevel: config.LOG_INFO,

        color: true,

        browsers: [ 'PhantomJS2' ],

        plugins: [
            "karma-jasmine",
            "karma-coverage",
            "karma-phantomjs2-launcher",
            "karma-spec-reporter",
            "karma-html-reporter",
            "karma-threshold-reporter"
        ],

        thresholdReporter: {
            statements: 90,
            branches: 80,
            functions: 80,
            lines: 90
        },

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: false,

        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: true
    });
};
