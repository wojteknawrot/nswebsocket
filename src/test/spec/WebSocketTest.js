describe("WebSocketService", function() {
    var scope,
        $ws,
        subscription,
        $webSocketBackend;

    var serverResponse = {
        xxx: "yyy"
    };
    var callback = {
        notify: function(message) {
        }
    };
    beforeEach(module("nsWs"));

    beforeEach(inject(function($rootScope, _$webSocketBackend_, _$ws_) {
        scope = $rootScope.$new();
        $webSocketBackend = _$webSocketBackend_;
        $ws = _$ws_;

        spyOn(callback, "notify");

        subscription = $ws.subscribe("/topic", callback.notify);
    }));

    it("callback should be fired with proper payload", function() {
        // when
        $webSocketBackend.publish("/topic", serverResponse);
        // then
        expect(callback.notify).toHaveBeenCalledTimes(1);
        expect(callback.notify).toHaveBeenCalledWith(serverResponse);
    });
    it("de-registered callback should not be fired", function() {
        // when
        subscription.unsubscribe();
        $webSocketBackend.publish("/topic", serverResponse);
        // then
        expect(callback.notify).not.toHaveBeenCalled();
    });
    it("callback should throw an Error when payload is not a JSON", function() {
        expect(function() {
            $webSocketBackend.publish("/topic", "{sds:1");
        }).toThrowError('bad JSON: "{sds:1"');
    });

    describe("send", function() {
        beforeEach(function() {
            // given
            spyOn($ws.$$stompClient, "send");
        });
        it("object should be converted to JSON", function() {
            // when
            $ws.send("/topic123", {}, { abc: "cba" });
            // then
            expect($ws.$$stompClient.send).toHaveBeenCalledWith("/topic123", {}, '{"abc":"cba"}');
        });
        it("send - string should be send as is", function() {
            // when
            $ws.send("/topic123", {}, "lalala");
            // then
            expect($ws.$$stompClient.send).toHaveBeenCalledWith("/topic123", {}, "lalala");
        });
    });

    describe("reconnect", function() {
        var stompClient;
        beforeEach(function() {
            stompClient = $ws.$$stompClient;
            $webSocketBackend.resetConnection();
            $webSocketBackend.connect();
        });

        it("stompClient instance should change", function() {
            expect($ws.$$stompClient).not.toBe(stompClient);
        });

        it("callback should be fired with proper payload", function() {
            // when
            $webSocketBackend.publish("/topic", serverResponse);
            // then
            expect(callback.notify).toHaveBeenCalledTimes(1);
            expect(callback.notify).toHaveBeenCalledWith(serverResponse);
        });

        it("de-registered callback should not be fired", function() {
            // when
            subscription.unsubscribe();
            $webSocketBackend.publish("/topic", serverResponse);
            // then
            expect(callback.notify).not.toHaveBeenCalled();
        });
    });
});
