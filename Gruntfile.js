module.exports = function(grunt) {

    var path = {
        target: 'dist'
    };

    require('grunt-task-loader')(grunt, {
        //customTasksDir: '__CUSTOM_DIR__',   // or ['__CUSTOM_DIR__'],
        mapping: {
            //taskA: 'another_tasks_dirs/', // custom task 'taskA' from custom tasks directory (load by grunt.loadTasks API)
            //taskB: 'ultraman/frog.js',    // custom task from file
            useminPrepare: 'grunt-usemin',
            ngtemplates: "grunt-angular-templates"
        }
    });


    var pkgJson = require('./package.json');
    require('time-grunt')(grunt);

    var config = grunt.file.readJSON('../nsWsConfig.json');

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        path: path,
        clean: ['dist'],

        concat: {
            options: {
                banner: '/*\n\tv<%= pkg.version %>\n\t<%= grunt.template.today("yyyy-mm-dd hh:mm") %>\n*/\n'
            },
            js: {
                src: [
                    'src/main/**/*.js'
                ],
                dest: 'dist/<%= pkg.name %>.js'
            },
            test: {
                src: [
                    'src/test/infrastructure/**/*.js'
                ],
                dest: 'dist/<%= pkg.name %>Test.js'
            }
        },

        uglify: {
            options: {
                banner: '/*\n\tv<%= pkg.version %>\n\t<%= grunt.template.today("yyyy-mm-dd hh:mm") %>\n*/\n'
            },
            build: {
                src: 'dist/<%= pkg.name %>.js',
                dest: 'dist/<%= pkg.name %>.min.js'
            }
        },

        karma:{
            watch: {
                configFile: 'src/test/conf.js',
                singleRun: false,
                autoWatch: true,
                flags: ['--remote-debugger-port=9000']
            },
            CI: {
                configFile: 'src/test/conf.js',
                singleRun: true,
                autoWatch: false,
                browsers: [ "PhantomJS2" ],
                reporters: [ "progress", "html", "coverage", "threshold" ],
                htmlReporter: {
                    outputDir: "<%= path.target %>/reports",
                    templatePath: null, // set if you moved jasmine_template.html
                    focusOnFailures: true, // reports show failures on start
                    namedFiles: false, // name files instead of creating sub-directories
                    pageTitle: null, // page title for reports; browser info by default
                    urlFriendlyName: false, // simply replaces spaces with _ for files/dirs
                    reportName: "unit", // report summary filename; browser info by default

                    // experimental
                    preserveDescribeNesting: false, // folded suites stay folded
                    foldAll: false // reports start folded (only with preserveDescribeNesting)
                },
                coverageReporter: {
                    dir: "<%= path.target %>/reports/",
                    reporters: [
                        { type: "html", subdir: "coverage-html" }
                    ]
                }

            }
        },
        copy: {
            deploy: {
                files: [
                    // includes files within path
                    {expand: true, flatten: true, src: [
                        'dist/nsWebSocket.js',
                        'dist/nsWebSocketTest.js'
                    ], dest: config.deployPath, filter: 'isFile'}
                ]
            }
        },
        bump: {
            options: {
                files: ["package.json", "bower.json"],
                updateConfigs: ["pkg", "bower"],
                commit: true,
                commitMessage: 'Release v%VERSION%',
                commitFiles: ["-a"],
                push: true,
                pushTo: 'origin'
            }
        }
    });

    //TODO extract release to grunt plugin
    grunt.registerTask('release', 'Release version', function() {

        var answer,
            currentVersion = pkgJson.version,
            inputStr = require('readline-sync'),
            generateDocs = grunt.option('docs'),
            version = grunt.option('ver') || grunt.option('version') || grunt.option('setversion');

        grunt.log.writeln("");
        grunt.log.writeln("Current version is " + currentVersion
            + " to release custom version: grunt release --ver=" + currentVersion + " [--docs]");

        if(!version){
            var newVersion =  currentVersion.substr(0, currentVersion.length-1) + (parseInt(currentVersion.substr(-1)) + 1);

            grunt.log.writeln("");
            answer = inputStr.question( 'Do you want to release: ' + newVersion
                + (generateDocs?" with documentation":"")
                + '? (y/yes) ');

            if (!answer.match(/^(yes|y)$/)) {
                grunt.log.writeln("Release cancelled.");
                return false;
            }

            version = newVersion;
        }

        if(version === currentVersion){
            grunt.log.writeln("");
            grunt.log.error("Version " + version + " has been already released. You cannot release same version twice.");
            grunt.log.writeln("");
            grunt.log.writeln("Release cancelled.");
            return false;
        }

        grunt.log.writeln("");
        grunt.log.writeln("Lets do a release: " + version);
        grunt.option("setversion", version);

        release.version = version;
        grunt.config.data.pkg.version = version;

        if( generateDocs ){
            grunt.task.run("clean", "build", "copy:dist", "copy:docsRelease", "gitadd:docs", "bump");
        } else {
            grunt.task.run("clean", "package", "test", "copy:dist", "bump");
        }

        // grunt.task.run calls async later
        grunt.log.writeln("");
        grunt.log.writeln("Released: " + version);
    });

    // Task(s) definition.
    grunt.registerTask('test', ['karma:CI']);
    grunt.registerTask('build', ['concat', 'uglify', 'test']);
    grunt.registerTask('install', ['build', 'copy:deploy']);
};
